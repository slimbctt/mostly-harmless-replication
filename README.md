

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">

> ##### 视频集锦：[论文精讲与重现](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) | [实证研究设计](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA) | [空间计量](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e) | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e) | [直击面板数据模型-Free](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)
> ##### 导航： 📍 [连享会主页](https://www.lianxh.cn)  | 📍 [知乎专栏](https://www.zhihu.com/people/arlionn/) | 📍 [直播课](http://lianxh.duanshu.com) 


&emsp;



**MHE！MHE！** 这是一本写给实证分析前线勇士的书。  

「[_Mostly Harmless Econometrics_](http://www.mostlyharmlesseconometrics.com/)；安格里斯特, 皮施克. 基本无害的计量经济学: 实证研究者指南 / (美) 安格里斯特, (美) 皮施克著 ; 郎金焕, 李井奎译.[M]. 2012.」一书各章的实现过程。

对照一下同样的任务同时用四种不同的软件或语言来写，是学习 Python 和 Stata 的绝佳资料。

### 书评

> “Finally – An econometrics book for practitioners! Not only for students, Mostly Harmless Econometricsis a fantastic resource for anyone who does empirical work.”       
— Sandra Black, UCLA

> “This is a remarkable book–it does the profession a great service by taking knowledge that is usually acquired over many years and distilling it in such a succinct manner.”        
— Amitabh Chandra, Harvard Kennedy School of Government

> “MHE is a fantastic book that should be read cover-to-cover by any young applied micro economist.  The book provides an excellent mix of statistical detail, econometric intuition and practical instruction.  The topic coverage includes the bulk of econometric tools used in the vast majority of applied microeconomics.  I wish there was an econometric textbook this well done when I was in graduate school.”     
— Bill Evans, University of Notre Dame


&emsp;

----

# Mostly Harmless Replication


![](https://images.gitee.com/uploads/images/2020/0223/104540_9351cbce_1522177.png)

## Synopsis

A bold attempt to replicate the tables and figures from the book [_Mostly Harmless Econometrics_](http://www.mostlyharmlesseconometrics.com/) in the following languages:
* Stata
* R
* Python
* Julia

## Chapters
1. Questions about _Questions_
2. The Experimental Ideal
3. [Making Regression Make Sense](03%20Making%20Regression%20Make%20Sense/03%20Making%20Regression%20Make%20Sense.md)
4. [Instrumental Variables in Action](04%20Instrumental%20Variables%20in%20Action/04%20Instrumental%20Variables%20in%20Action.md)
5. [Parallel Worlds](05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data.md)
6. [Getting a Little Jumpy](06%20Getting%20a%20Little%20Jumpy/06%20Getting%20a%20Little%20Jumpy.md)
7. [Quantile Regression](07%20Quantile%20Regression/07%20Quantile%20Regression.md)
8. [Nonstandard Standard Error Issues](08%20Nonstandard%20Standard%20Error%20Issues/08%20Nonstanard%20Standard%20Error%20Issues.md)

## Getting started
Check out [Getting Started](https://github.com/vikjam/mostly-harmless-replication/wiki/Getting-started) in the Wiki for tips on setting up your machine with each of these languages.

## Contributions
Feel free to submit [pull requests](https://github.com/blog/1943-how-to-write-the-perfect-pull-request)!

&emsp; 
&emsp; 

&emsp;

![](https://images.gitee.com/uploads/images/2020/0816/235448_8627f8ed_1522177.png)

&emsp;

## 相关课程

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  
> <img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[DSGE 专题](https://gitee.com/arlionn/DSGE)** | 朱传奇| [线上直播 4 天](https://www.lianxh.cn/news/c2f757a200474.html) <br> 2020年9月 19-20日; 26-27日  |
| &#x2B55; **[Stata数据清洗](https://lxh.360dhf.cn/live/detail/4114)** | 游万海| [直播, 2 小时](https://www.lianxh.cn/news/f785de82434c1.html)，已上线 |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh底部推文海报.png)


&emsp;


&emsp; 

> #### [连享会·直播 - DSGE 专题](https://gitee.com/arlionn/DSGE)     
> **线上直播 4 天**：2020.9.19-20; 9.26-27  
> **主讲嘉宾**：朱传奇 (中山大学)  
>    &emsp;    
> **课程主页**：<https://gitee.com/arlionn/DSGE> 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-DSGE-海报600.png)

&emsp;

&emsp;


&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。


---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")



--- - --

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

